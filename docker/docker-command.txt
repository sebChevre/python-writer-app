# Create volume
docker volume create python-writer-app

# launch with volume mapping
docker run -e PYTHONUNBUFFERED=1  -v python-writer-app:/opt/simple-python-writer/data loop --nbIteration 12 --delay 3

//volume content on mac docker
mac: docker run -it --rm --privileged --pid=host busybox nsenter -t1 -m -u -i -n

//volume content on windows docker
\\wsl$\docker-desktop-data\version-pack-data\community\docker\volumes\
