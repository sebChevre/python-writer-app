#!/usr/bin/python3
# Import the necessary modules
from ast import arg
import datetime
import time
import os
import socket
import argparse
import constant

# global
nbIterations = 5
iterationDelay = 2
itertationCounter = 0

## constants
DIR_NAME = "data"
FILE_PATH = "./" + DIR_NAME + "/date.out"

## function
def localISO8601Date():
    return datetime.datetime.now().isoformat()

def hostname():
    return socket.gethostname()

def buildLine():
    return (f"[counter: {itertationCounter}] - [{localISO8601Date()}] - [{hostname()}]\n")

def log(msg):
     print("[" + localISO8601Date() + "] - " + msg)

def parseAndSetArguments ():
    parser = argparse.ArgumentParser()
    parser.add_argument('--nbIteration', required=True, help='Number of writing iterations')
    parser.add_argument('--delay', required=True, help='Delay beetween each writing itertations')
    args = parser.parse_args()
    
    log("nbIteration args: " + args.nbIteration)
    global nbIterations
    nbIterations = int(args.nbIteration)
    
    log("delay args: " + args.delay)
    global iterationDelay
    iterationDelay = int(args.delay)
    


def main () :
    global itertationCounter
    
    parseAndSetArguments()

    if not os.path.exists(constant.DIR_NAME):
        os.mkdir(constant.DIR_NAME)
        log("Directory " + constant.DIR_NAME + " dont exist. Application will create it.")
        log("File date.out will be created too")
    else:
        log("Directory " + constant.DIR_NAME + " exist, using it.")

        if os.path.isfile(constant.FILE_PATH):
            log("File date.out already exist, reuse it")
            log("File size: " + str(os.path.getsize(constant.FILE_PATH)) + "ko")
        else:
            log("File date.out dont exist. Application will create it")
    
    log("Starting process...")
 
    loopCounter = 0

    while (loopCounter < nbIterations) :
        ## Set the hostname and the current date
        host = socket.gethostname()
        date = localISO8601Date()
        ## Convert the date output to a string
        now = str(date)
        ## Open the file named date in append mode
        ## Append the output of hostname and time
        
        f = open(constant.FILE_PATH,"a" )
        f.write(buildLine())
        #f.write(now +" - ")
        #f.write(host +"\n")
        f.close()
        ## Sleep for five seconds then continue the loop
        time.sleep(iterationDelay)
        loopCounter +=1
        itertationCounter += 1
        log("loop")

if __name__ == "__main__":
    main()

