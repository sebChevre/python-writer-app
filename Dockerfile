FROM python:alpine3.14
WORKDIR /opt/simple-python-writer
ADD simple-writer-loop.py .
ADD constant.py .
## RUN pip install pystrich
ENTRYPOINT [ "python", "/opt/simple-python-writer/simple-writer-loop.py" ]