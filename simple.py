#!/usr/bin/python3
# Import the necessary modules
import time
import os
import socket

## function
def dateAsStr():
    return time.strftime("%Y-%m-%d%H:%M:%S")

def log(msg):
     print("[" + dateAsStr() + "] - " + msg)
## constants
DIR_NAME = "data"
FILE_PATH = "./" + DIR_NAME + "/date.out"

if not os.path.exists(DIR_NAME):
    os.mkdir(DIR_NAME)
    log("Directory " + DIR_NAME + " dont exist. Application will create it.")
    log("File date.out will be created too")
else:
    log("Directory " + DIR_NAME + " exist, using it.")

    if os.path.isfile(FILE_PATH):
        log("File date.out already exist, reuse it")
        log("File size: " + str(os.path.getsize(FILE_PATH)) + "ko")
    else:
        log("File date.out dont exist. Application will create it")


log("Starting process...")
while True :
    ## Set the hostname and the current date
    host = socket.gethostname()
    date = dateAsStr()
    ## Convert the date output to a string
    now = str(date)
    ## Open the file named date in append mode
    ## Append the output of hostname and time
    
    f = open(FILE_PATH,"a" )
    
    f.write(now +" - ")
    f.write(host +"\n")
    f.close()
     ## Sleep for five seconds then continue the loop
    time.sleep(5)

